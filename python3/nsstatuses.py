import subprocess
import sys

def isAppNotActive(status):
   return (status != 'Active')
 
def checkAppStatus(appToCheck, apps):
    for (app, status, age) in apps:
        appNotActive = isAppNotActive(status)
        if (app == appToCheck):
            if appNotActive:
                return (1, 'FAIL')
            else:
                return (0, 'SUCCESS')

    return (1, 'FAIL')

def parseNSAppsAndStatus():
    apps = []
    status = subprocess.check_output(['kubectl', 'get', 'ns'])
    statusArray = status.splitlines()

    for s in statusArray:
        sdecode = s.decode('ascii')
        apps.append(sdecode.split())

    return apps
