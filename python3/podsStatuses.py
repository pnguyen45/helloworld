import subprocess
import sys

def isPodNotActive(status):
   return (status != 'Running')
 
def checkPodsStatus(pods, appToCheck):
    if (len(pods) == 0):
        return (1, 'No pods for app: ' + appToCheck)
 
    for (name, ready, status, restart, age) in pods:
        if (name == "NAME"):
            continue

        podNotActive = isPodNotActive(status)
        
        if podNotActive:
            return (1, 'FAIL')

    return (0, 'SUCCESS') 

def parsePodsAndStatus(appToCheck):
    pods = []
    status = subprocess.check_output(['kubectl', 'get', 'pods', '-n',  appToCheck])

    if (status == 'No resources found.'):
        return pods
    else:
        statusArray = status.splitlines()

        for s in statusArray:
            sdecode = s.decode('ascii')
            pods.append(sdecode.split())

    return pods
