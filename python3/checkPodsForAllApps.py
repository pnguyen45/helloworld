import subprocess
import podsStatuses

appsToCheck = ['ci', 'kyc', 'ls', 'helmtms']

for appToCheck in appsToCheck:
    pods = podsStatuses.parsePodsAndStatus(appToCheck)
    print(appToCheck, ':', podsStatuses.checkPodsStatus(pods, appToCheck))
