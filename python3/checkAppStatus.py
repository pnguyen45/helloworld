import nsstatuses 
import sys 

appsFromNS = nsstatuses.parseNSAppsAndStatus()
appToCheck = sys.argv[1] 

print(appToCheck, ":", nsstatuses.checkAppStatus(appToCheck, appsFromNS))
