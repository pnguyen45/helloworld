import sys
import podsStatuses

appToCheck = sys.argv[1]
pods = podsStatuses.parsePodsAndStatus(appToCheck)

print(appToCheck, ':', podsStatuses.checkPodsStatus(pods, appToCheck))
