import nsstatuses 

appsFromNS = nsstatuses.parseNSAppsAndStatus()
appsToCheck = ['ci', 'kyc', 'ls', 'helmtms']

for appToCheck in appsToCheck:
    print(appToCheck, ":", nsstatuses.checkAppStatus(appToCheck, appsFromNS))
